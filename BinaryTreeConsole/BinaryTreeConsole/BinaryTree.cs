﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTreeConsole
{
    class BinaryTree<T> : ICollection<T> where T : IComparable<T>
    {
        // корень
        private BinaryTreeNode<T> _head;
        public BinaryTreeNode<T> Head
        {
            get { return _head; }
            private set { _head = value; }
        }
        // количество узлов
        private int _count;
        public int Count { get {return _count; } }

        // добавление узла в дерево       
        public void Add(T value)
        {

            if (_head == null)
            {
                _head = new BinaryTreeNode<T>(value);
            }
            else
            {
                AddTo(_head, value);
            }
            _count++;
        }

        private void AddTo(BinaryTreeNode<T> node, T value)
        {            
            if(value.CompareTo(node.Value) < 0)
            {
                if (node.Left == null)
                {
                    node.Left = new BinaryTreeNode<T>(value);
                }
                else
                {
                    AddTo(node.Left, value);
                }
            }
            else
            {
                if (node.Right == null)
                {
                    node.Right = new BinaryTreeNode<T>(value);
                }
                else
                {
                    AddTo(node.Right, value);
                }
            }
        }

        // содержит ли дерево узел
        public bool Contains (T value)
        {
            BinaryTreeNode<T> parent;
            return FindWithParent(value, out parent) != null;
        }

        // поиск первого узла с искомым значением, а также его родителя
        private BinaryTreeNode<T> FindWithParent(T value,out BinaryTreeNode<T> parent)
        {
            BinaryTreeNode<T> current = _head;
            parent = null;

            while(current != null)
            {
                int result = current.CompareTo(value);

                if (result > 0)
                {
                    parent = current;
                    current = current.Left;
                }
                else if (result < 0)
                {
                    parent = current;
                    current = current.Right;
                }
                else  break;              
            }
            return current;
        }

        // удаление узла      
        public bool Remove(T value)
        {
            BinaryTreeNode<T> parent;
            BinaryTreeNode<T> current;

            // поиск удаляемого значения в дереве
            current = FindWithParent(value, out parent);
            // если значения в дереве нет, возвращаем false
            if (current == null) return false;
            
            _count--;

            // 1-й вариант: у найденного узла нет правого потомка
            if (current.Right == null)
            {
                // если это корень - удаляем его. Левый потомок - новый корень.
                if (parent == null)
                {
                    _head = current.Left;
                }
                else
                {
                    // сравним значение родителя со значением удаляемого узла
                    int result = parent.CompareTo(current.Value);

                    if (result > 0)
                    {
                        // Если удаляемый узел левый
                        parent.Left = current.Left;
                    }
                    else if (result < 0)
                    {
                        // если удаляемый узел правый
                        parent.Right = current.Left;
                    }
                }
            }
            // 2-й вариант: у удаляемого узла есть правый потомок, у которого в свою очередь нет левого потомка
            else if (current.Right.Left == null)
            {
                // левое поддерево удалямого узла становится левым поддеревом левого потомка удаляемого узла
                current.Right.Left = current.Left;

                if (parent == null)
                {
                    _head = current.Right;
                }
                // определяем: правым или левым узлом для родителя был удаляемый узел
                else
                {
                    int result = parent.CompareTo(current.Value);

                    if (result < 0)
                    {
                        parent.Left = current.Right;
                    }
                    else if (result > 0)
                    {
                        parent.Right = current.Right;
                    }
                }
            }
            // 3-й вариант: у удаляемого узла есть правый потомок, а у него - левый
            else
            {
                // самый левый потомок правого поддерева удаляемого элемента
                BinaryTreeNode<T> leftmost = current.Right.Left;
                // его родитель 
                BinaryTreeNode<T> leftmostParent = current.Right;

                // поиск самого левого потомка правого поддерева (и его родителя)
                while(leftmost.Left != null)
                {
                    leftmostParent = leftmost;
                    leftmost = leftmost.Left;
                }

                // "извлекаем" самый левый узел
                leftmostParent.Left = leftmost.Right;
                // и "вставляем" его вместо удаляемого
                leftmost.Left = current.Left;
                leftmost.Right = current.Right;

                // как обычно, определяем: удаляемый узел - корень? а если нет: правый или левый он потомок?
                if (parent == null)
                {
                    _head = leftmost;
                }
                else
                {
                    int result = parent.CompareTo(current.Value);

                    if (result > 0)
                    {
                        parent.Left = leftmost;
                    }
                    if (result < 0)
                    {
                        parent.Right = leftmost;
                    }
                }
            }

            // узел удален
            return true;
        }

        // перечислители для обобщенного и не обобщенного IEnumerable
        public IEnumerator<T> GetEnumerator()
        {
            return recInOrder(_head).GetEnumerator();
        }
        System.Collections.IEnumerator  System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        

        // симметричный обход дерева
        public IEnumerable<T> InOrder()
        {
            if (_head == null) yield break;
            
                // помещаем текущий узел в стек
                Stack<BinaryTreeNode<T>> stack = new Stack<BinaryTreeNode<T>>();
                BinaryTreeNode<T> current = _head;

                bool goLeftNext = true;

                stack.Push(current);

                // пока в стеке есть значения, продолжаем обход дерева
                while (stack.Count > 0)
                {
                    // если разрешено искать левый элемент, находим крайний левый элемент
                    if(goLeftNext)
                    {
                        while(current.Left != null)
                        {
                            stack.Push(current);
                            current = current.Left;
                        }
                    }
                    // возвращаем крайний левый элемент или его брата или родителя
                    yield return current.Value;

                    if (current.Right != null)
                    {
                        current = current.Right;
                        goLeftNext = true;
                    }
                    else
                    {
                        current = stack.Pop();
                        goLeftNext = false;
                    }

               }
        }

        // прямой обход дерева
        public IEnumerable<T> PreOrder()
        {
            Stack<BinaryTreeNode<T>> stack = new Stack<BinaryTreeNode<T>>();
            BinaryTreeNode<T> current = _head;

            while(current != null)
            {
                // если у узла есть два потомка - добавляем его в стек, чтоб после обхода
                // его левого поддерева вернуться к правому
                if (current.Right != null && current.Left != null)
                {
                    stack.Push(current);
                }
                yield return current.Value;

                current = current.Left ?? current.Right;

                // если у текущего узла нет ни правого ни левого потомка, 
                // возвращаемся к последнему узлу, у которого есть правый потомок
                // (а точнее, сразу к его правому потомку)
                if (current == null)
                {
                    if (stack.Count == 0)  break;                 
                    else current = stack.Pop().Right;                    
                }

            }

        }

        // обратный обход дерева
        public IEnumerable<T> PostOrder()
        {
            if (_head == null) yield break;

            var stack = new Stack<BinaryTreeNode<T>>();
            var current = _head;

            while (stack.Count > 0 || current != null)
            {
                if (current == null)
                {
                    current = stack.Pop();
                    if (stack.Count > 0 && current.Right == stack.Peek())
                    {
                        stack.Pop();
                        stack.Push(current);
                        current = current.Right;
                    }
                    else { yield return current.Value; current = null; }
                }
                else
                {
                    if (current.Right != null) stack.Push(current.Right);
                    stack.Push(current);
                    current = current.Left;
                }
            }
        }

        // поперечный обход дерева
        public IEnumerable<T> LevelOrder()
        {
            if (_head == null) yield break;

            var queue = new Queue<BinaryTreeNode<T>>();
            queue.Enqueue(_head);

            while (queue.Count > 0)
            {
                var node = queue.Dequeue();

                yield return node.Value;

                if (node.Left != null)    queue.Enqueue(node.Left);
                if (node.Right != null)   queue.Enqueue(node.Right);
            }
        }

        // минимальное значение
        public T MinValue
        {
            get
            {
                if (_head == null)  throw new InvalidOperationException("Tree is empty");
                var current = _head;
                while (current.Left != null)  current = current.Left;
                return current.Value;
            }
        }

        //максимальное значение
        public T MaxValue
        {
            get
            {
                if (_head == null)  throw new InvalidOperationException("Tree is empty");
                var current = _head;
                while (current.Right != null) current = current.Right;
                return current.Value;
            }
        }

        // добавление коллекции
        public void AddRange(IEnumerable<T> collection)
        {
            foreach (var value in collection)
                Add(value);
        }

        // очистить дерево
        public void Clear()
        {
            _head = null;
            _count = 0;
        }

        // копирование узлов дерева в массив с указанного индекса массива
        public void CopyTo(T[] array, int arrayIndex)
        {
            foreach (var value in this)
                array[arrayIndex++] = value;
        }

        public virtual bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        public IEnumerable<T> recInOrder(BinaryTreeNode<T> node)
        {           
            if (node.Left != null) foreach (var ch in recInOrder(node.Left)) yield return ch;
            yield return node.Value;
            if (node.Right != null) foreach (var ch in recInOrder(node.Right)) yield return ch;
        }

        
    }
}
