﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTreeConsole
{
    class BinaryTreeNode<TNode>:IComparable<TNode> where TNode:IComparable<TNode>
    {
        // Node's value
        public TNode Value { get; private set; }
        // Left node (descendant)
        public BinaryTreeNode<TNode> Left { get; set; }
        //
        public BinaryTreeNode<TNode> Right { get; set; }

        // Implementation IComparable
        public int CompareTo(TNode other)
        {
            // Comparison node's value to other value
            return Value.CompareTo(other);
        }

        public int CompareNode(BinaryTreeNode<TNode> other)
        {
            // Comparison node's value to other node's value
            return Value.CompareTo(other.Value);
        }

        // ctor
        public BinaryTreeNode(TNode value)
        {
            Value = value;
        }



    }
}
