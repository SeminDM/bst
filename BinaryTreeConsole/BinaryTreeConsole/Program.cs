﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTreeConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            BinaryTree<int> myTree = new BinaryTree<int>();

            myTree.Add(100);
            myTree.Add(150);
            myTree.Add(50);
            myTree.Add(30);
            myTree.Add(75);
            myTree.Add(200);
            myTree.Add(25);
            myTree.Add(40);
            myTree.Add(80);
            myTree.Add(250);
            myTree.Add(20);
            myTree.Add(27);
            myTree.Add(35);
            myTree.Add(77);
            myTree.Add(230);
            myTree.Add(300);
            myTree.Add(21);
            myTree.Add(26);
            myTree.Add(31);
            myTree.Add(37);
            myTree.Add(220);
            myTree.Add(235);
            myTree.Add(22);

            //Console.WriteLine("Min value:" + myTree.Min());
            //Console.WriteLine("Max value:" + myTree.Max());
            //Console.WriteLine("Count:" + myTree.Count);
            //Console.WriteLine("Preorder traversal:");
            //Console.WriteLine(string.Join(" ", myTree.PreOrder()));
            //Console.WriteLine("Inorder traversal:");
            //Console.WriteLine(string.Join(" ", myTree.InOrder()));
            Console.WriteLine("Recursive Inorder traversal:");
            Console.WriteLine(string.Join(" ", myTree.recInOrder(myTree.Head)));
            Console.WriteLine("Postorder traversal:");
            Console.WriteLine(string.Join(" ", myTree.PostOrder()));
            Console.WriteLine("Levelorder traversal:");
            Console.WriteLine(string.Join(" ", myTree.LevelOrder()));
            



            Console.ReadKey();
            
        }
    }   
}
